﻿namespace RandomFwLib
{
    public static class Library
    {
        private static (int buyDcCount, double buyDcSum) GetPartnerDayBuyDC()
        {
            double buyDCSum = 8;
            int buyDCCount = 15;

            return (buyDCCount, buyDCSum);
        }

        private static (int sellDcCount, double sellDcSum) GetPartnerDaySellDC()
        {
            double sellDCSum = 5;
            int sellDCCount = 10;

            return (sellDCCount, sellDCSum);
        }

        public static int GetPartnerInfo()
        {
            var (buyDCTodayCount, buyDCTodaySum) = GetPartnerDayBuyDC();
            var (sellDCTodayCount, sellDCTodaySum) = GetPartnerDaySellDC();

            return buyDCTodayCount + sellDCTodayCount;
        }
    }
}